using Godot;
using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;

public class DeltaDriver
{
	private Communication comm = new Communication();
	public void set_mode(string mode)
	{
		// Ustawia tryb "T1", "AUTO" (uppercase!)
	}
	public string get_mode()
	{
		// Zwraca aktualny tryb (na STM32!) "T1" "AUTO" (uppercase!)
		return "T1";
	}
	public void set_reference(string cord)
	{
		// Ustawia układ odniesień "JOINT", "WORLD" (uppercase!)
	}
	public string get_reference()
	{
		// Zwraca aktualny układ odniesień (na STM32) "JOINT", "WORLD" (uppercase!)
		return "JOINT";
	}
	public int get_status()
	{
		// Zwraca aktualny status (na STM32)
		return 0;
	}
	public void connect()
	{
		comm.connect();
		// Wykonuje próbe połączenia z STM32
	}
	public bool is_connect()
	{
		return comm.is_connected();
	}
	public void ack_fault()
	{
		// Zatwierdza błąd w STM32
	}
	public int get_fault_code()
	{
		// Zwraca kod błędu
		return 0;
	}
	public void move(int j1, int j2, int j3)
	{
		// Porusza robotem zależnie od układu odniesień
		// WORLD j1 -> X, j2 -> Y, j3 -> Z
		// j1,j2,j3 należy do zbioru {-1, 0, 1}
	}
	public void set_velocity(double vel)
	{
		// Ustawia predkośc poruszania się robotem
	}
	public double get_velocity()
	{
		// Zwraca prędkośc poruszania robotem
		return 0;
	}
	public double get_max_velocity()
	{
		// Zwraca maksymalną prędkośc poruszania robotem
		return 0;
	}
	public double[,] get_position()
	{
		// Zwraca pozycje robota w postaci tablicy:
		// {
		//		{posX, poxY, posZ}, pozycja efektora
		//		{jog0, jog1, jog2}, nachylenie ramion
		// }
		
		double[,] val = new double[2,3];
		return val;
	}
	public void prog_run()
	{
		// Załącza program (w tybie "AUTO")
	}
	public void prog_stop()
	{
		// Stopuje program (w tybie "AUTO")
	}
	public bool prog_load(string prog)
	{
		// Ładuje program "prog" do STM32
		return false;
	}
	public string get_prog_name()
	{
		// Zwraca nazwę programu na STM32
		return "";
	}
	public void send(string msg)
	{
		comm.send(msg);
	}
	public void on_quit()
	{
		comm.disconnect();
	}
	public void start_recieving()
	{
		comm.start_recieving();
	}
	public bool is_recieving()
    {
		return comm.is_recieving();	
    }
}
