using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

public class Vision
{
	private UDPClientHandler vision_client = new UDPClientHandler();
	public bool is_connect()
	{
		return vision_client.is_connected();
		//return false;
	}
	public void connect()
	{
		vision_client.Connect();
	}

	public void on_quit()
	{
		vision_client.on_quit();
	}

	public void get_current_frame()
	{
		vision_client.recieve();
	}
}
