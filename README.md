Godot app for controling Delta Robot created by "Control Engineers" science club at Silesian University of Technology. 

Features:
- Sending commands to Robot
- Recieving information from Robot
- Recieving image from camera (slow, needs to be optimized)
- Simulating sending commands to robot (linear motion, jog motion, joint motion, circular motion)
- Python script for sending iamges to Godot

Note: Stub directory needs to be outside Deltaapp directory for it to work
