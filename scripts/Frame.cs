using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Frame
{
	double a = 0;   //rotation angle around Z axis
	double b = 0;   //rotation angle around Y axis
	double c = 0;   //rotation angle around X axis

	double x, y, z;

	double sa, ca, sb, cb, sc, cc;  // sinuses and cosinuses of a, b, c angles

	double[,] rotMat;
	double[,] transMat;

	String name = "new_frame";
	public Frame(string _name, double _x, double _y, double _z, double _a, double _b, double _c)
	{
		name = _name;
		x = _x;
		y = _y;
		z = _z;
		a = _a * Math.PI / 180.0;
		b = _b * Math.PI / 180.0;
		c = _c * Math.PI / 180.0;
		sa = Math.Sin(a);
		ca = Math.Cos(a);
		sb = Math.Sin(b);
		cb = Math.Cos(b);
		sc = Math.Sin(c);
		cc = Math.Cos(c);

		rotMat_update();
		transMat_update();
	}

	public void set_z_angle(double angle)
	{
		a = angle * Math.PI / 180.0;
		sa = Math.Sin(a);
		ca = Math.Cos(a);
		rotMat_update();
	}

	public void set_y_angle(double angle)
	{
		b = angle * Math.PI / 180.0;
		sb = Math.Sin(b);
		cb = Math.Cos(b);
		rotMat_update();
	}

	public void set_x_angle(double angle)
	{
		c = angle * Math.PI / 180.0;
		sc = Math.Sin(c);
		cc = Math.Cos(c);
		rotMat_update();
	}

	private void rotMat_update()
	{

		rotMat = new double[3, 3] {   { ca*cb,  ca*sb*sc - sa*cc,   ca*sb*cc + sa*sc },
										{ sa*cb,  sa*sb*sc + ca*cc,   sa*sb*cc - ca*sc },
										{ -sb,               cb*sc,              cb*cc } };

	}

	private void transMat_update()
	{
		transMat = new double[3, 1] { { x }, { y }, { z } };
	}

	public void set_pos_x(double x)
	{
		this.x = x;
		transMat_update();
	}

	public void set_pos_y(double y)
	{
		this.y = y;
		transMat_update();
	}

	public void set_pos_z(double z)
	{
		this.z = z;
		transMat_update();
	}

	public void set_rot(double rot_x, double rot_y, double rot_z)
	{
		a = rot_z;
		b = rot_y;
		c = rot_x;
		rotMat_update();
	}



	public double[] get_point_pos(double x, double y, double z)
	{
		//zwraca pozycje punktu w tym ukladzie wspolrzednych
		double[] diffPos = new double[3];
		diffPos[0] = x - transMat[0, 0];
		diffPos[1] = y - transMat[1, 0];
		diffPos[2] = z - transMat[2, 0];

		diffPos = matMultiplyRotMat(rotMat, diffPos);
		//dopisac jeszcze mnozenie
		return diffPos;
	}

	public double[,] get_frame_pos(double x, double y, double z)
	{

		return transMat;
	}

	public double[] get_move_vector(double[] vec)
	{

		return matMultiplyRotMat(rotMat, vec);
	}

	private double[] matMultiplyRotMat(double[,] m1, double[] m2)
	{
		int a = 3;
		int b = 3;
		double[] m1m2 = new double[3];
		for (int i = 0; i < a; i++)
		{
			for (int j = 0; j < b; j++)
			{
				m1m2[i] += m1[i, j] * m2[j];
			}

		}
		return m1m2;
	}

}
