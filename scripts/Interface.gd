extends Control

const Space = preload("res://scenes/Space.tscn");
const Vision = preload("res://scenes/Vision.tscn");
var MENU = {"T1":0,"AUTO":1,"SIM":2,"WORLD":3,"JOINT":4, "UF_1":5 };

var moving = [0,0,0];
var sim_command_preffix : String = "LIN";

var vel_idx : int;
const vel_tab = [0.01, 0.02, 0.03, 0.04, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0];

var scene_idx : int;
var scene_tab = [Space.instance(), Vision.instance()];
const scene_name = ["Delta", "Visual system"];

const ClassDeltaView = preload("res://scripts/DeltaView.gd");
var DeltaView = ClassDeltaView.new(scene_tab[0].get_node("Delta"));

const ClassVision = preload("res://scripts/Vision.gd");
var ScriptVison = ClassVision.new()

#controller vars
var move_plus = ["move_x+", "move_y+", "move_z+"]
var move_minus = ["move_x-", "move_y-", "move_z-"]
onready var Frame = $Frame
onready var RefMenu = $Frame/Main/ControlBox/ManualBox/MoveBox/RefMenu
onready var ModeMenu = $Frame/HeadBar/ModeBox/ModeMenu
onready var Status = $Frame/HeadBar/ModeBox/Status
onready var VisionLight = $Frame/HeadBar/ConnectBox/CenterVision/VisionLight
onready var ControlerLight = $Frame/HeadBar/ConnectBox/CenterControl/ControlLight
onready var FaultInfo = $Frame/HeadBar/FaultBox/Fault
onready var WindowSettings = $WindowSettings
onready var WindowSettingsBox = $WindowSettings/Container
onready var Position = $Frame/Main/ControlBox/PositionBox
onready var Velocity = $Frame/Main/ControlBox/ManualBox/VelocityBox/Velocity
onready var ActProg = $Frame/Main/ControlBox/ProgramBox/ActProg
onready var FileOpen = $FileDialog
onready var ViewName = $Frame/Main/ViewBox/ViewSwitchBox/Name
onready var View = $Frame/Main/ViewBox/View/Viewport
onready var ConnectionButton = $Frame/HeadBar/ConnectBox/Connect
onready var TCPBox = $Frame/Main/ControlBox/TCPBox
onready var SyncBox = $Frame/Main/ControlBox/TCPBox/SyncBox
onready var SimulationBox = $Frame/Main/ControlBox/SimulationBox
onready var UF_Pos = $Frame/Main/ControlBox/UF_Frame_Tests/Robot_UF_Pos
onready var UF_Pos_write = $Frame/Main/ControlBox/UF_Frame_Tests/UF_Frame_Pos
onready var UF_Rot_write = $Frame/Main/ControlBox/UF_Frame_Tests/UF_Frame_Rot
onready var console = $Console_Window
onready var Ef_Stat# = $Frame/Main/ControlBox/RelABBOX/Status_Container/Status

func status_info( _status ) -> String:
	# Przypisuje do numeru statusu odpowiedni tekst
#	match fault:
#		1:
#			return "SINGULARITY (ikine: NO_SOLUTION)";
#		0:
#			return "";
	return "UNKNOW ERROR";


func fault_info( fault ) -> String:
	# Przypisuje do numeru błędu odpowiedni tekst
	match fault:
		1:
			return "SINGULARITY (ikine: NO_SOLUTION)";
		0:
			return "";
	return "UNKNOW ERROR";


func velocity_set():
	var vel_max = Proxy.get_max_velocity();
	Proxy.set_velocity(vel_max * vel_tab[vel_idx]);
	Velocity.text = String(vel_tab[vel_idx] * 100) + "%";


func scene_set():
	var scene = scene_tab[scene_idx];
	var prev_scene = View.get_child(0);

	View.remove_child(prev_scene);
	View.add_child(scene);
	ViewName.text = scene_name[scene_idx];


func connect_checked():
	var color_connected = Color(0,1,0);
	var color_disconnected = Color(1,0,0);
	var style_controler = StyleBoxFlat.new();
	var style_vision = StyleBoxFlat.new();

	if(Proxy.is_controler_connected()):
		style_controler.set_bg_color(color_connected);
	else:
		style_controler.set_bg_color(color_disconnected);

	if(Proxy.is_vision_connected()):
		style_vision.set_bg_color(color_connected);
	else:
		style_vision.set_bg_color(color_disconnected);

	ControlerLight.add_stylebox_override("panel",style_controler);
	VisionLight.add_stylebox_override("panel",style_vision);

func sync_checked():
	var color_syncing = Color(0,1,0);
	var color_not_syncing = Color(1,0,0);
	var style_syncer = StyleBoxFlat.new();
	if(SyncBox.get_node("Syncer").pressed):
		style_syncer.set_bg_color(color_syncing);
	else:
		style_syncer.set_bg_color(color_not_syncing);
	SyncBox.get_node("SyncLight").add_stylebox_override("panel",style_syncer);

func _ready():
	# Setup Mode

	var idx;
	var mode = "T1";
	Proxy.set_mode(mode);
	ModeMenu.get_popup().add_item("T1", MENU["T1"]);
	ModeMenu.get_popup().add_item("AUTO", MENU["AUTO"]);
	ModeMenu.get_popup().add_item("SIM", MENU["SIM"]);
	ModeMenu.get_popup().connect("id_pressed", self, "_on_ModeMenu_selected");
	idx = ModeMenu.get_popup().get_item_index(MENU[Proxy.get_mode()]);
	ModeMenu.text = ModeMenu.get_popup().get_item_text(idx);

	# Setup Cords
	var ref = "JOINT";
	Proxy.set_reference(ref);
	RefMenu.get_popup().add_item("WORLD", MENU["WORLD"]);
	RefMenu.get_popup().add_item("JOINT", MENU["JOINT"]);
	RefMenu.get_popup().add_item("UF_1", MENU["UF_1"]);
	RefMenu.get_popup().connect("id_pressed", self, "_on_RefMenu_selected");
	idx = RefMenu.get_popup().get_item_index(MENU[Proxy.get_reference()]);
	RefMenu.text = RefMenu.get_popup().get_item_text(idx);

	# Setup Status
	Status.text = status_info(Proxy.get_status());
	# Setup Velocity
	vel_idx = 0;
	velocity_set();
	# Setup ActProg
	ActProg.text = Proxy.get_loaded_prog_name();
	# Connect Checked
	connect_checked();
	# Setup View
	scene_idx = 0;
	scene_set();
	OS.window_resizable=true;
	#OS.window_fullscreen=true;
	OS.set_window_maximized(true)
	#Setup console
	console.visible = false
func _process(_delta):
	controller_input();
	# Update Move
	var moving_changed = false;

	for item in moving:
		if(item != 0):
			moving_changed = true;
			break;

	if(Proxy.get_mode()!="SIM"):
		Proxy.robot_move(moving[0], moving[1], moving[2]);
	elif(Proxy.get_mode()=="SIM" and moving_changed):
		var str_comp = [];
		for i in range(3):
			var comp_string
			if(moving[i]<0):
				comp_string = String(moving[i])
			else:
				comp_string = String("+"+String(moving[i]))
			str_comp.append(comp_string)
		
		Proxy.sim_command(String("JOG"+String(str_comp[0])+String(str_comp[1])+String(str_comp[2])+"TOOL_"));
		
	if(Proxy.get_mode()=="T1" and Proxy.is_controler_connected() and moving_changed):
		var str_comp = [];
		for i in range(3):
			var comp_string
			if(moving[i]<0):
				comp_string = String(moving[i])
			else:
				comp_string = String("+"+String(moving[i]))
			str_comp.append(comp_string)
		Proxy.send(String("JOG"+String(str_comp[0])+String(str_comp[1])+String(str_comp[2])+"TOOL_"));
	
	if(Proxy.get_mode()=="SIM" and Proxy.get_reference()=="JOINT"):
		sim_command_preffix = "JNT";
	else:
		sim_command_preffix = "LIN";
	# Update DeltaView
	if(Proxy.get_view_updated() == false):
		var params = Proxy.get_robot_position();
		var robot_var = [
			[params[0], params[1], params[2]],
			[params[3], params[4], params[5]]
		];
		#print(robot_var)
		if(robot_var):
			var _integrity = DeltaView.update(robot_var);
		Proxy.set_view_updated(true);
	if(Input.is_action_pressed("ui_cancel")):
		get_tree().quit();
	#connect_checked();
	sync_checked();
	if(Proxy.is_vision_connected()):
		Proxy.update_current_frame();
	#Proxy.sim_exec_trace();
	
	if Input.is_action_just_pressed("toggle_console"):
		console.visible = true
		
func _on_RefMenu_selected(id):
	# Zmienia układ odniesienia robota
	var keys = MENU.keys();
	Proxy.set_reference(keys[id]);
	var idx = RefMenu.get_popup().get_item_index(MENU[Proxy.get_reference()]);
	RefMenu.text = RefMenu.get_popup().get_item_text(idx);


func _on_ModeMenu_selected(id):
	# Zmienia tryb pracy robota
	var keys = MENU.keys();
	Proxy.set_mode(keys[id]);
	var idx = ModeMenu.get_popup().get_item_index(MENU[Proxy.get_mode()]);
	ModeMenu.text = ModeMenu.get_popup().get_item_text(idx);



func _on_Connect_pressed():
	# Wykonuje próbę połącznienia do Kontrolera i systemu wizyjnego
	if(!Proxy.is_controler_connected() and !Proxy.is_vision_connected()):
		Proxy.connect_controler();
		#Proxy.connect_vision();
		if(Proxy.is_controler_connected()):
			connect_checked();
			#Proxy.get_current_frame();
			ConnectionButton.text="Disconnect";
	else:
		Proxy.on_quit();
		connect_checked();
		ConnectionButton.text="Connect";

func _on_Refresh_timeout():
	# Odświerza funkcje, statusy o niskim priorytecie
	# Aktualizacja Statusu
	Status.text = status_info(Proxy.get_status());
	# Sprawdza połącznie
	connect_checked();
	# Aktualizuje błąd
	FaultInfo.text = fault_info(Proxy.get_fault_code());
	# Aktualizuje pozycje
	# Pobiera pozycję z robota

	var pos = Proxy.get_robot_position();
	View.get_parent().get_node("ActPositionCode").text = Proxy.current_position;

	Position.get_node("X").text = String(round(pos[2]));
	Position.get_node("Y").text = String(round(pos[0]));
	Position.get_node("Z").text = String(round(pos[1]));
	Position.get_node("J1").text = String(pos[3] * 180 / PI);
	Position.get_node("J2").text = String(pos[4] * 180 / PI);
	Position.get_node("J3").text = String(pos[5] * 180 / PI);
	var x = int(pos[2])*10;
	var y = int(pos[0])*10; #mnożone razy 10 by uzyskać wartości takie jak w robocie
	var z = int(pos[1])*10;
	var Strx = String(x);
	var Stry = String(y);
	var Strz = String(z);
	if(x>0):
		var Dl = 4-len(Strx)
		Strx = "+"+"0".repeat(Dl)+String(x);
	else:
		var Dl = 5-len(Strx)
		Strx = "-"+"0".repeat(Dl)+String(-x);
	if(y>0):
		var Dl = 4-len(Stry)
		Stry = "+"+"0".repeat(Dl)+String(y);
	else:
		var Dl = 5-len(Stry)
		Stry = "-"+"0".repeat(Dl)+String(-y);
	if(z>0):
		var Dl = 4-len(Strz)
		Strz = "+"+"0".repeat(Dl)+String(z);
	else:
		var Dl = 5-len(Strz)
		Strz = "-"+"0".repeat(Dl)+String(-z);
	if(len(Strx)>4):
		Strx = Strx.substr(0,5);
	if(len(Stry)>4):
		Stry = Stry.substr(0,5);
	if(len(Strz)>4):
		Strz = Strz.substr(0,5);
	View.get_parent().get_node("PositionCode").text = Strx+Stry+Strz;



func _on_FaultAck_pressed():
	# Akceptuje aktualny błąd
	Proxy.ack_fault();


func _on_Settings_pressed():
	# Aktywuje okno popup z ustawiniami
	var rect = Frame.get_rect().grow_individual(-500, -200, -500, -200);
	WindowSettings.popup(rect);
	WindowSettingsBox.set_position(Vector2(0,0));
	WindowSettingsBox.set_size(rect.size)


func _on_J1Up_button_down():
	# Porusza robotem J1/X-, zbocze narastajace
	moving[0] = 1;
	
func _on_J2Up_button_down():
	# Porusza robotem J2/Y-
	moving[1] = 1;


func _on_J3Up_button_down():
	# Porusza robotem J3/Z-
	moving[2] = 1;


func _on_J1Down_button_down():
	# Porusza robotem J1/X+
	moving[0] = -1;


func _on_J2Down_button_down():
	# Porusza robotem J2/Y+
	moving[1] = -1;


func _on_J3Down_button_down():
	# Porusza robotem J3/Z+
	moving[2] = -1;


func _on_VelUp_pressed():
	# Zwieksza prędkośc poruszania
	vel_idx += 1;
	if( vel_idx > vel_tab.size() - 1 ):
		vel_idx = vel_tab.size() - 1;
	velocity_set();


func _on_VelDown_pressed():
	# Zminiejsza predkośc poruszania
	vel_idx -= 1;
	if( vel_idx < 0 ):
		vel_idx = 0;
	velocity_set();


func _on_Run_pressed():
	# Załącznie pracy robota - wykonywanie programu
	Proxy.robot_run();


func _on_Stop_pressed():
	# Zatrzymanie pracy robota - reset cyklu
	Proxy.robot_stop();


func _on_Load_pressed():
	# Open FileDialog
	var rect = Frame.get_rect().grow_individual(-200, -200, -200, -200);
	FileOpen.popup(rect);


func _on_FileDialog_confirmed():
	# Open, Valid and Load program to Controler
	var path = FileOpen.current_path
	if(Proxy.load_prog_from_file(path)):
		ActProg.text = FileOpen.current_file;


func _on_Prev_pressed():
	# Zmienie scene na poprzednią
	scene_tab[scene_idx] = View.get_child(0);
	scene_idx -= 1;
	if( scene_idx < 0 ):
		scene_idx = scene_tab.size() - 1;
	scene_set();


func _on_Next_pressed():
	# Zmienie scene na następną
	scene_tab[scene_idx] = View.get_child(0);
	scene_idx += 1;
	if( scene_idx > scene_tab.size() - 1 ):
		scene_idx = 0;
	scene_set();


func _on_J1Up_button_up():
	# Zatrzymanie poruszania robotem, zbocze opadajace
	moving[0] = 0;


func _on_J2Up_button_up():
	# Zatrzymanie poruszania robotem
	moving[1] = 0;


func _on_J3Up_button_up():
	# Zatrzymanie poruszania robotem
	moving[2] = 0;


func _on_J1Down_button_up():
	# Zatrzymanie poruszania robotem
	moving[0] = 0;


func _on_J2Down_button_up():
	# Zatrzymanie poruszania robotem
	moving[1] = 0;


func _on_J3Down_button_up():
	# Zatrzymanie poruszania robotem
	moving[2] = 0;

func NOTIFICATION_WM_QUIT_REQUEST():
	Proxy.on_quit();

func _on_DllButton_pressed():
	Proxy.get_current_frame();
	#Proxy.start_recieving();


func _on_Position_button_pressed():
	if(Proxy.is_controler_connected() and Proxy.driver_is_recieving()):
		Proxy.send("G_P");
		View.get_parent().get_node("ActPositionCode").text = Proxy.current_position;
	else:
		View.get_parent().get_node("ActPositionCode").text = "Something went wrong...";

func _on_Recieving_Button_pressed():
	#Proxy.get_current_frame();
	Proxy.start_recieving();


func _on_ActPos_refresh_timeout():
	if(Proxy.is_controler_connected() and Proxy.driver_is_recieving()):
		Proxy.send("G_P");
		View.get_parent().get_node("ActPositionCode").text = Proxy.current_position;
	else:
		View.get_parent().get_node("ActPositionCode").text = "Something went wrong...";

func _on_Exec_trace_timeout():
	Proxy.sim_exec_trace(); # Replace with function body.


func _on_exec_command_1_pressed():
	Proxy.sim_command(String(sim_command_preffix+SimulationBox.get_node("SimCommand1").text+"TOOL_"+ Console.velocity));
	if(Proxy.is_controler_connected()):
		Proxy.send(String(sim_command_preffix+SimulationBox.get_node("SimCommand1").text+"TOOL_"+ Console.velocity));
	print(String(sim_command_preffix+SimulationBox.get_node("SimCommand1").text+"TOOL_"+ Console.velocity))

func _on_exec_command_2_pressed():
	Proxy.sim_command(String(sim_command_preffix+SimulationBox.get_node("SimCommand2").text+"TOOL_"+ Console.velocity));
	if(Proxy.is_controler_connected()):
		Proxy.send(String(sim_command_preffix+SimulationBox.get_node("SimCommand2").text+"TOOL_"+ Console.velocity));

func _on_exec_command_3_pressed():
	Proxy.sim_command(String(sim_command_preffix+SimulationBox.get_node("SimCommand3").text+"TOOL_"+ Console.velocity));
	if(Proxy.is_controler_connected()):
		Proxy.send(String(sim_command_preffix+SimulationBox.get_node("SimCommand3").text+"TOOL_"+ Console.velocity));


func _on_Circ_pressed():
	Proxy.sim_command(String("CIR"+"A"+SimulationBox.get_node("SimCommand2").text+"E"+SimulationBox.get_node("SimCommand3").text+"TOOL_"+ Console.velocity))
	if(Proxy.is_controler_connected()):
		Proxy.send(String("CIR"+"A"+SimulationBox.get_node("SimCommand2").text+"E"+SimulationBox.get_node("SimCommand3").text+"TOOL_"+ Console.velocity));

func _on_save_button_1_pressed():
	SimulationBox.get_node("SimCommand1").text = View.get_parent().get_node("PositionCode").text;


func _on_save_button_2_pressed():
	SimulationBox.get_node("SimCommand2").text = View.get_parent().get_node("PositionCode").text;


func _on_save_button_3_pressed():
	SimulationBox.get_node("SimCommand3").text = View.get_parent().get_node("PositionCode").text;


func _on_Pos_X_text_changed():
	Proxy.set_uf_pos_x(UF_Pos_write.get_node("Pos_X").text.to_float());
	rebase_axes_aux();
	uf_update();


func _on_Pos_Y_text_changed():
	Proxy.set_uf_pos_y(UF_Pos_write.get_node("Pos_Y").text.to_float());
	rebase_axes_aux();
	uf_update();

func _on_Pos_Z_text_changed():
	Proxy.set_uf_pos_z(UF_Pos_write.get_node("Pos_Z").text.to_float());
	rebase_axes_aux();
	uf_update();

func _on_Rot_X_text_changed():
	Proxy.set_uf_rot_x(UF_Rot_write.get_node("Rot_X").text.to_float());
	rebase_axes_aux();
	uf_update();

func _on_Rot_Y_text_changed():
	Proxy.set_uf_rot_y(UF_Rot_write.get_node("Rot_Y").text.to_float());
	rebase_axes_aux();
	uf_update();

func _on_Rot_Z_text_changed():
	Proxy.set_uf_rot_z(UF_Rot_write.get_node("Rot_Z").text.to_float());
	rebase_axes_aux();
	uf_update();
	
func rebase_axes_aux():
	var vect_pos = Vector3(UF_Pos_write.get_node("Pos_X").text.to_float(),UF_Pos_write.get_node("Pos_Z").text.to_float(),-UF_Pos_write.get_node("Pos_Y").text.to_float());
	var vect_rot = Vector3(UF_Rot_write.get_node("Rot_Y").text.to_float(),UF_Rot_write.get_node("Rot_Z").text.to_float(),UF_Rot_write.get_node("Rot_X").text.to_float());
	DeltaView.rebase_axes(vect_pos,vect_rot);
	uf_update();

func uf_update():
	var pos = Proxy.get_robot_position();
	var uf_pos = Proxy.get_point_pos(pos[0], pos[2], pos[1]);
	UF_Pos.get_node("UF_X").text = String(round(uf_pos[0]));
	UF_Pos.get_node("UF_Y").text = String(round(uf_pos[1]));
	UF_Pos.get_node("UF_Z").text = String(round(uf_pos[2]));



func _on_Syncer_pressed():
	pass # Replace with function body.
	
func controller_input():
	for i in range(3):
		if Input.is_action_just_pressed(move_plus[i]) :
			moving[i] = 1;
		if Input.is_action_pressed(move_minus[i]):
			moving[i] = -1;
		if Input.is_action_just_released(move_plus[i]) or Input.is_action_just_released(move_minus[i]):
			moving[i] = 0;
			
func _on_Grab_pressed():
	Ef_Stat.text = "On"

func _on_Release_pressed():
	Ef_Stat.text = "Off"
