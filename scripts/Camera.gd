extends Camera

onready var console = get_node("/root/Interface/Console_Window")

const ZOOM_SPEED = 5;
const ZOOM_MAX = 1;
const ZOOM_MIN = 1000;
const MOUSE_MOVE_SPEED = 1;
const KEY_MOVE_SPEED = 100;
const ROT_SPEED = 0.0001 * 180 / PI;
###############################################################################
export(float, 0.0, 1.0) var sensitivity = 0.25

var CameraState = true;

# Mouse state
var _mouse_position = Vector2(0.0, 0.0)
var _total_pitch = 0.0

# Movement state
var _direction = Vector3(0.0, 0.0, 0.0)
var _velocity = Vector3(0.0, 0.0, 0.0)
var _acceleration = 30
var _deceleration = -10
var _vel_multiplier = 450

# Keyboard state
var _w = false
var _s = false
var _a = false
var _d = false
var _q = false
var _e = false

###############################################################################
var CameraClass = load("res://scripts/CameraOrbit.gd");
var orbitCam = CameraClass.new(
	self.transform.basis.z * (-300),
	self.transform.origin,
	self.transform.basis.y,
	ZOOM_MIN,
	ZOOM_MAX
	);



func _input(event):
	if CameraState == true:
		if event is InputEventMouseButton:
			match event.button_index:
				BUTTON_WHEEL_UP:
					orbitCam.zoom(ZOOM_SPEED * event.factor);
				BUTTON_WHEEL_DOWN:
					orbitCam.zoom(-ZOOM_SPEED * event.factor);
		if event is InputEventMouseMotion:
			match event.button_mask:
				BUTTON_MASK_MIDDLE:
					var move = MOUSE_MOVE_SPEED * event.relative;
					orbitCam.move(move.x, move.y);
				BUTTON_MASK_RIGHT:
					var rot = ROT_SPEED * event.relative;
					orbitCam.rotate_yaw(rot.x);
					orbitCam.rotate_pitch(rot.y);
		if event is InputEventJoypadMotion:
			if(event.axis == 2 and (event.axis_value > 0.5 or event.axis_value < -0.5)):
				var rot = ROT_SPEED * event.axis_value;
				orbitCam.rotate_yaw(rot);
		look_at_from_position(orbitCam.eye, orbitCam.target, orbitCam.up);
	else:
		# Receives mouse motion
		if event is InputEventMouseMotion:
			_mouse_position = event.relative
	
	# Receives mouse button input
	if event is InputEventMouseButton:
		match event.button_index:
			BUTTON_RIGHT: # Only allows rotation if right click down
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED if event.pressed else Input.MOUSE_MODE_VISIBLE)
			
	# Receives key input
	if event is InputEventKey and console.visible == false:
		match event.scancode:
			KEY_W:
				_w = event.pressed
			KEY_S:
				_s = event.pressed
			KEY_A:
				_a = event.pressed
			KEY_D:
				_d = event.pressed
			KEY_Q:
				_q = event.pressed
			KEY_E:
				_e = event.pressed

func _process(delta):
	if Input.is_action_just_pressed("cam_switch_mode") == true:#change camera mode
		CameraState = not CameraState
	if CameraState == true:
		var moved = false;
		if((Input.is_action_pressed("cam_front") or Input.is_action_pressed("move_cam_front")) and console.visible == false):
			orbitCam.move(0, KEY_MOVE_SPEED * delta);
			moved = true;
		if((Input.is_key_pressed(KEY_S) or Input.is_action_pressed("move_cam_back")) and console.visible == false):
			orbitCam.move(0, -KEY_MOVE_SPEED * delta);
			moved = true;
		if((Input.is_key_pressed(KEY_A) or Input.is_action_pressed("move_cam_left")) and console.visible == false):
			orbitCam.move(-KEY_MOVE_SPEED * delta, 0);
			moved = true;
		if((Input.is_key_pressed(KEY_D) or Input.is_action_pressed("move_cam_right")) and console.visible == false):
			orbitCam.move(KEY_MOVE_SPEED * delta, 0);
			moved = true;
		var events = [
		Input.get_action_strength("rot_cam_up"),
		Input.get_action_strength("rot_cam_down"),
		Input.get_action_strength("rot_cam_right"),
		Input.get_action_strength("rot_cam_left")
		]
		orbitCam.rotate_pitch( ROT_SPEED * (events[0]-events[1]));
		orbitCam.rotate_yaw( ROT_SPEED * (events[2]-events[3]));
		if(moved):
			look_at_from_position(orbitCam.eye, orbitCam.target, orbitCam.up);
	else:
		_update_mouselook()
		_update_movement(delta)

func _update_movement(delta):
	# Computes desired direction from key states
	_direction = Vector3(_d as float - _a as float, 
						 _e as float - _q as float,
						 _s as float - _w as float)
	
	# Computes the change in velocity due to desired direction and "drag"
	# The "drag" is a constant acceleration on the camera to bring it's velocity to 0
	var offset = _direction.normalized() * _acceleration * _vel_multiplier * delta \
		+ _velocity.normalized() * _deceleration * _vel_multiplier * delta
	
	# Checks if we should bother translating the camera
	if _direction == Vector3.ZERO and offset.length_squared() > _velocity.length_squared():
		# Sets the velocity to 0 to prevent jittering due to imperfect deceleration
		_velocity = Vector3.ZERO
	else:
		# Clamps speed to stay within maximum value (_vel_multiplier)
		_velocity.x = clamp(_velocity.x + offset.x, -_vel_multiplier, _vel_multiplier)
		_velocity.y = clamp(_velocity.y + offset.y, -_vel_multiplier, _vel_multiplier)
		_velocity.z = clamp(_velocity.z + offset.z, -_vel_multiplier, _vel_multiplier)
	
		translate(_velocity * delta)

# Updates mouse look 
func _update_mouselook():
	# Only rotates mouse if the mouse is captured
	if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		_mouse_position *= sensitivity
		var yaw = _mouse_position.x
		var pitch = _mouse_position.y
		_mouse_position = Vector2(0, 0)
		
		# Prevents looking up/down too far
		#pitch = clamp(pitch, -90 - _total_pitch, 90 - _total_pitch)
		#_total_pitch += pitch
	
		rotate_y(deg2rad(-yaw))
		rotate_object_local(Vector3(1,0,0), deg2rad(-pitch))



