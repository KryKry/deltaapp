extends Control

onready var input_box = $console_box/input_box/input
onready var output_box = $console_box/output_box/output
onready var console_box = $console_box
var command_handler = load("res://scripts/Command_handler.gd").new()
var selected = false
var TXT
var length
var vel = " "
var spacebar_index
var i
var test
var test2
var velocity = "V0000"
var velmax = 70

func _ready():
	pass
	
func process_command(text):
	var words = text.split(" ")
	words = Array(words)
	
	for _j in range(words.count("")):
		words.erase("")
	
	if words.size() == 0:
		return
	
	output_text(text)
	
	commands(text)
	
func output_text(text):
	output_box.text = str(output_box.text, "\n", text)
	
func _on_input_text_entered(new_text):
	input_box.clear()
	process_command(new_text)
	
func commands(text):
	match(text):
		"help":
			TXT = command_handler.help()
			output_text(TXT)
		"clear":
			output_box.text = ""
		"save_point":
			TXT = command_handler.save_point()
			output_text(TXT)
	if "velocity" in text:
		vel = " "
		length = text.length()
		spacebar_index = text.find(" ")
		for i in range (spacebar_index , length):
			vel = vel + text[i]
		test = vel as float
		if test > 0 and test <= 100:
			if test < 1.0:
				test = 1.0
			vel = (round((test/100)*velmax)) as String	
			output_text("Set velocity " + vel)
			test2 = vel as float
			if test2 >= 10:
				Console.velocity = "V00"+vel
			if test2 >0 and test2 < 10:
				Console.velocity = "V000" + vel
		if test <= 0 or test > 100:
			output_text("Incorrect value")

func clear():
	if "help" in output_box.text:
		output_box.text = ""
		output_box.cursor_set_line(0)
