class_name CameraOrbit

var eye : Vector3;
var target : Vector3;
var forward : Vector3;
var up : Vector3;
var right : Vector3;
var ZOOM_MAX : float;
var ZOOM_MIN : float;

func _init(ntarget : Vector3, neye : Vector3, nup : Vector3, zmin := 300, zmax := 1):
	target = ntarget;
	eye = neye;
	up = nup;
	ZOOM_MAX = zmax;
	ZOOM_MIN = zmin;
	update();

func move(x : float, z : float):
	var go_front = Vector3( forward.x, 0, forward.z ).normalized();
	var go_right = Vector3( right.x, 0, right.z ).normalized();
	
	eye += z * (-go_front) + (-go_right) * x;
	target += z * (-go_front) + (-go_right) * x;

func zoom(focus : float):
	var zoom = focus * (-forward);
	var dist = eye - target + zoom;
	
	if((focus > 0 && dist.length() > ZOOM_MAX) || (focus < 0 && dist.length() < ZOOM_MIN)):
		eye += zoom;
	
func rotate_yaw(yaw : float):
	var axis = Vector3(0, 1, 0);
	var p_eye = eye - target
	var p_up = p_eye + up;
	
	p_eye 	= rot(p_eye, axis, yaw);
	p_up	= rot(p_up, axis, yaw);

	eye = target + p_eye;
	up = p_up - p_eye;
	update();

func rotate_pitch(pitch : float):
	var axis = right;
	var p_eye = eye - target
	var p_up = p_eye + up;
	
	
	p_up	= rot(p_up, axis, pitch);
	p_eye 	= rot(p_eye, axis, pitch);

	var nup = p_up - p_eye;	
	if(nup.dot(Vector3(0, 1, 0)) > 0):
		eye = target + p_eye;
		up = nup;
		update();

	
func rot(point : Vector3, axis : Vector3, arg : float) -> Vector3:
	var M = [
		[ 
			axis.x * axis.x * (1 - cos(arg)) + cos(arg),
			axis.x * axis.y * (1 - cos(arg)) - axis.z * sin(arg),
			axis.x * axis.z * (1 - cos(arg)) + axis.y * sin(arg) ],
		[
			axis.x * axis.y * (1 - cos(arg)) + axis.z * sin(arg),
			axis.y * axis.y * (1 - cos(arg)) + cos(arg),
			axis.y * axis.z * (1 - cos(arg)) - axis.x * sin(arg) ],
		[
			axis.x * axis.z * (1 - cos(arg)) - axis.y * sin(arg),
			axis.y * axis.z * (1 - cos(arg)) + axis.x * sin(arg),
			axis.z * axis.z * (1 - cos(arg)) + cos(arg) ]
	return Vector3(
		point.x * M[0][0] + point.y * M[1][0] + point.z * M[2][0],
		point.x * M[0][1] + point.y * M[1][1] + point.z * M[2][1],
		point.x * M[0][2] + point.y * M[1][2] + point.z * M[2][2]
		);

func update():
	forward = Vector3(eye - target).normalized();
	right = Vector3( forward.cross(up) ).normalized();
	
