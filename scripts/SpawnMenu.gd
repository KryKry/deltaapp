extends Button

signal AddRandomObjectButton
signal SpawnSnickserButton
signal Spawn3BitButton
signal SpawnMarsButton
signal RemoveAllObjectButton
signal VacuumStateButton


func _on_AddRandomObject_pressed():
	emit_signal("AddRandomObjectButton")

func _on_SpawnSnickers_pressed():
	emit_signal("SpawnSnickserButton")

func _on_Spawn3Bit_pressed():
	emit_signal("Spawn3BitButton")

func _on_SpawnMars_pressed():
	emit_signal("SpawnMarsButton")

func _on_RemoveAllObjects_pressed():
	emit_signal("RemoveAllObjectButton")

func _on_VacuumState_pressed():
	emit_signal("VacuumStateButton")


