using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;


public class Proxy : Node
{
	private bool updated = false;
	private DeltaModel model;
	private DeltaDriver driver;
	private ProgReader reader;
	private Vision vision;
	private Communication coms;
	public static Byte[] current_frame = new byte[306720];
	public static Byte[] recieved_frame = new byte[306720];
	public static string current_position = string.Empty;
	Proxy()
	{
		DeltaParams param = new DeltaParams();
		param.rb = 204.3;//188.2;//203.4;
		param.re = 45;
		param.r1 = 280;
		param.r2 = 590;
		model = new DeltaModel(param);
		driver = new DeltaDriver();
		reader = new ProgReader();
		vision = new Vision();
	}
	bool get_view_updated()
	{
		return updated;
	}
	void set_view_updated(bool val)
	{
		this.updated = val;
	}
	void set_mode(string mode) {
		if(is_controler_connected())
		{
			model.set_mode(mode);
		}
		else
		{
			model.set_mode(mode);
		}
	}
	string get_mode() {
		if(is_controler_connected())
		{
			return model.get_mode();
		}
		else
		{
			return model.get_mode();
		}
	}
	void set_reference(string cord) {
		if(is_controler_connected())
		{
			model.set_reference(cord);
		}
		else
		{
			model.set_reference(cord);
		}
	}
	string get_reference()
	{
		if(is_controler_connected())
		{
			return model.get_reference();
		}
		else
		{
			return model.get_reference();
		}
	}
	int get_status()
	{
		if(is_controler_connected())
		{
			return driver.get_status();
		}
		else
		{
			return model.get_status();
		}
	}
	private double to_num_5(string value)
    {
		double num = 0;
		num += char.GetNumericValue(value[1]) * 100;
		num += char.GetNumericValue(value[2]) * 10;
		num += char.GetNumericValue(value[3]) * 1;
		num += char.GetNumericValue(value[4]) * 0.1;
		if (value[0] == '-')
			num = -num;
		return num;
    }
	private int to_num_2(string value)
	{
		int num = (int)char.GetNumericValue(value[1]);
		if (value[0] == '-')
			num = -num;
		return num;
	}
	void connect_controler()
	{
		try{
		driver.connect();
		}
		catch(System.Exception)
		{
			//disconnect();
			//throw;
		}
	}
	void connect_vision()
	{
		vision.connect();
	}
	bool is_controler_connected()
	{
		return driver.is_connect();
	}
	bool is_vision_connected()
	{
		return vision.is_connect();
	}
	void ack_fault()
	{
		if(is_controler_connected())
		{
			driver.ack_fault();
		}
		model.ack_fault();
	}
	int get_fault_code()
	{
		if(is_controler_connected())
		{
			return driver.get_fault_code();
		}
		else
		{
			return model.get_fault_code();
		}
	}
	void robot_move(int j1, int j2, int j3)
	{
		if(j1 != 0 || j2 != 0 || j3 != 0)
		{
			if(is_controler_connected())
			{
				model.move(j1, j2, j3);
			}
			else
			{
				model.move(j1, j2, j3);
			}
			updated = false;
		}
	}
	void sim_move_to(double j1, double j2, double j3)
	{
		if (j1 != 0 || j2 != 0 || j3 != 0)
		{
			if (is_controler_connected())
			{
				model.move_to(j1, j2, j3);
			}
			else
			{
				model.move_to(j1, j2, j3);
			}
			updated = false;
		}
	}
	void sim_exec_trace()
	{
		if (String.Equals(this.model.get_mode(), "SIM"))
		{
			model.exec_trace();
			updated = false;
		}
	}

	void sim_command(string command)
    {
		model.command(command);
		updated = false;
    }

	void set_velocity(float vel)
	{
		if(is_controler_connected())
		{
			driver.set_velocity(vel);
		}
		else
		{
			model.set_velocity(vel);
		}
	}
	float get_velocity()
	{
		if(is_controler_connected())
		{
			return (float)driver.get_velocity();
		}
		else
		{
			return (float)model.get_velocity();
		}
	}
	float get_max_velocity()
	{
		if(is_controler_connected())
		{
			return (float)driver.get_max_velocity();
		}
		else
		{
			return (float)model.get_max_velocity();
		}
	}
	float[] get_robot_position()
	{
		double[][] vars_2d = new double[2][];
		float[] vars = new float[6];
		if(is_controler_connected())
		{
			//vars_2d = driver.get_position();
			vars_2d = model.get_position();
		}
		else
		{
			vars_2d = model.get_position();
		}

/*		for(int i = 0; i < 2; i++)
		{*/
			vars[0] = (float)vars_2d[0][1];   // xgod = y
			vars[1] = (float)vars_2d[0][2];	// ygod = z
			vars[2] = (float)vars_2d[0][0];   // zgod = x
			vars[3] = (float)vars_2d[1][0];   //j1
			vars[4] = (float)vars_2d[1][1];   //j2
			vars[5] = (float)vars_2d[1][2];	//j3
/*		}*/
		return vars;
	}
	void robot_run()
	{
		driver.prog_run();
	}
	void robot_stop()
	{
		driver.prog_stop();
	}
	bool load_prog_from_file(string path)
	{
		string prog = reader.load(path);
		return driver.prog_load(prog);
	}
	string get_loaded_prog_name()
	{
		return driver.get_prog_name();
	}
	void set_settings(string[] settings)
	{
		// TODO
		// setp for DeltaModel
	}
	void send(string msg)
	{
		driver.send(msg);
	}
	void start_recieving()
	{
		driver.start_recieving();
	}
	bool driver_is_recieving()
    {
		return driver.is_recieving();
    }
	void on_quit()
	{
		Console.WriteLine("Quit");
		driver.on_quit();
		vision.on_quit();
	}
	void get_current_frame()
	{
		vision.get_current_frame();
	}

	void update_current_frame()
	{
		current_frame = recieved_frame;
	}

	public float[] get_point_pos(double x, double y, double z)
	{
		double[] uf_pos = model.user_frame.get_point_pos(x, y, z);
		float[] float_uf_pos = { (float)uf_pos[1], (float)uf_pos[2], (float)uf_pos[0] };
		return float_uf_pos;
	}

	public void set_uf_pos_x(float x)
	{
		model.user_frame.set_pos_x(x);
	}

	public void set_uf_pos_y(float y)
	{
		model.user_frame.set_pos_y(y);
	}

	public void set_uf_pos_z(float z)
	{
		model.user_frame.set_pos_z(z);
	}

	public void set_uf_rot_x(float x)
	{
		model.user_frame.set_x_angle(x);
	}

	public void set_uf_rot_y(float y)
	{
		model.user_frame.set_y_angle(y);
	}

	public void set_uf_rot_z(float z)
	{
		model.user_frame.set_z_angle(z);
	}

};
