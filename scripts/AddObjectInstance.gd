extends Spatial

var Mars= preload("res://res/Scene/Mars.tscn")
var Bit= preload("res://res/Scene/3Bit.tscn")
var Snickers= preload("res://res/Scene/Snickers.tscn")
var Papaj= preload("res://res/Scene/Papaj.tscn")

var Vacuum_State = false
var candybar_list
var candybar 
var candybars = [Mars, Bit, Snickers]

func _ready():
	var spawn_menu_node = get_node("/root/Interface/SpawnMenu")
	spawn_menu_node.connect("AddRandomObjectButton", self, "_on_AddRandomObject_pressed")
	spawn_menu_node.connect("SpawnSnickserButton", self, "_on_SpawnSnickserButton_pressed")
	spawn_menu_node.connect("Spawn3BitButton", self, "_on_Spawn3BitButton_pressed")
	spawn_menu_node.connect("SpawnMarsButton", self, "_on_SpawnMarsButton_pressed")
	spawn_menu_node.connect("RemoveAllObjectButton", self, "_on_RemoveAllObjects_pressed")
	spawn_menu_node.connect("VacuumStateButton", self, "_on_VacuumState_pressed")

func _on_AddRandomObject_pressed():
	randomize() 
	var timeDict = OS.get_time();
	candybar_list = candybars[randi()% candybars.size()]
	candybar = candybar_list.instance()
	candybar.set_global_transform(Transform(Basis(), Vector3(rand_range(-300,200),rand_range(-653,-647),rand_range(-300,300))))
	candybar.rotate_y(rand_range(0,359))
	get_node("Objects").add_child(candybar)
	if timeDict.hour == 21 and timeDict.minute == 37:
		var Papaj_instance = Papaj.instance()
		#Papaj_instance.global_transform.origin = Vector3(rand_range(-300,200),-500,rand_range(-300,300))
		Papaj_instance.set_global_transform(Transform(Basis(), Vector3(rand_range(-300,200),-500,rand_range(-300,300))))
		get_node("Objects").add_child(Papaj_instance)

func _on_SpawnSnickserButton_pressed():
	randomize() 
	candybar = Snickers.instance()
	candybar.set_global_transform(Transform(Basis(), Vector3(rand_range(-300,200),rand_range(-653,-647),rand_range(-300,300))))
	candybar.rotate_y(rand_range(0,359))
	get_node("Objects").add_child(candybar)

func _on_Spawn3BitButton_pressed():
	randomize() 
	candybar = Bit.instance()
	candybar.set_global_transform(Transform(Basis(), Vector3(rand_range(-300,200),rand_range(-653,-647),rand_range(-300,300))))
	candybar.rotate_y(rand_range(0,359))
	get_node("Objects").add_child(candybar)

func _on_SpawnMarsButton_pressed():
	randomize() 
	candybar = Mars.instance()
	candybar.set_global_transform(Transform(Basis(), Vector3(rand_range(-300,200),rand_range(-653,-647),rand_range(-300,300))))
	candybar.rotate_y(rand_range(0,359))
	get_node("Objects").add_child(candybar)

func _on_RemoveAllObjects_pressed():
	var Objects = get_node("Objects")
	Objects = Objects.get_children()
	for candybar_instance in Objects:
		candybar_instance.queue_free()

func _on_VacuumState_pressed():
	Vacuum_State = not Vacuum_State

func _process(_delta):
	if Input.is_action_just_pressed("vacuum_mode") == true:
		Vacuum_State = not Vacuum_State #vacuum state
	var timeDict = OS.get_time();
	if Input.is_action_just_pressed("spawn_object") == true and timeDict.hour == 21 and timeDict.minute == 37:
		var Papaj_instance = Papaj.instance()
		Papaj_instance.set_global_transform(Transform(Basis(), Vector3(rand_range(-300,200),-500,rand_range(-300,300))))
		#Papaj_instance.global_transform.origin = Vector3(rand_range(-300,200),-500,rand_range(-300,300))
		get_node("Objects").add_child(Papaj_instance)
	
	elif Input.is_action_just_pressed("spawn_object") == true:
			randomize() 
			candybar_list = candybars[randi()% candybars.size()]
			candybar = candybar_list.instance()
			candybar.set_global_transform(Transform(Basis(), Vector3(rand_range(-300,200),rand_range(-653,-647),rand_range(-300,300))))
			candybar.rotate_y(rand_range(0,359))
			get_node("Objects").add_child(candybar)
	
	var suction_cup = get_node("Delta/Effector/Suction_cup")
	var suction_pos = suction_cup.get_global_transform().origin
	var Objects = get_node("Objects")
	
	Objects = Objects.get_children()
	if Vacuum_State == true:
		#print(get_node("Objects").get_children())
		for candybar_instance in Objects:
			#print(candybar_instance)
			var candybar_pos = candybar_instance.get_global_transform().origin
			#print(candybar_pos)
			var distance = suction_pos.distance_to(candybar_pos)
			if distance < 30 :
				candybar_instance.global_transform.origin = (suction_pos - Vector3(0,3,0))
	if Input.is_action_just_pressed("ClearObjList") == true:
		for candybar_instance in Objects:
			candybar_instance.queue_free()


