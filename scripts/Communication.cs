﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;

//TCP
 public class Communication {
    public void connect()
    {
        try
        {
            tcp_client = new TcpClient();
            tcp_client.Connect("192.168.0.155",10);
            Console.WriteLine("tcp_client connected");
            stream = tcp_client.GetStream();
            //tcp_client.NoDelay = true;
            //start_recieving();
        }
        catch (Exception e)
        {
            disconnect();
            Console.WriteLine(e.Message);
        }
    }
    
    public void disconnect()
    {
        if (reciever != null){
            reciever.Abort();
        }
        tcp_client.Close();
        stream.Close();
    }
    
    public bool is_connected()
    {
        bool retval;
        try
        {
            retval = tcp_client.Connected;
        }
         catch(System.Exception)
        {
            retval = false;
        }
        return retval;
    }
    public void send(string msg)
    {
        stream = tcp_client.GetStream();
        last_message_time = DateTime.Now;
        elapsed = last_message_time - last_send_time;    
        if(is_connected() && elapsed.TotalMilliseconds > 100)
        {
            byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(msg);
            Console.WriteLine("TCP: sending to server : " + msg);
            stream.Write(bytesToSend,0,bytesToSend.Length);
            last_send_time = DateTime.Now;  
        }
    }
    public void recv()
    {
        while (true)
        {       
            try
            {
                stream = tcp_client.GetStream();
                byte[] recieved_bytes = new Byte[256];
                byte[] recieved_bytes_header = new Byte[3];
                String recieved_message = String.Empty;
                String recieved_message_header = String.Empty;

                Int32 bytes = stream.Read(recieved_bytes, 0, recieved_bytes.Length);

                recieved_message_header = System.Text.Encoding.ASCII.GetString(recieved_bytes, 0, 3);
                if (String.Equals(recieved_message_header, "POS"))
                {
                    recieved_message = System.Text.Encoding.ASCII.GetString(recieved_bytes, 0, bytes);
                    Proxy.current_position = recieved_message;
                } 
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());
                Proxy.current_position = "Something went wrong...";
            }
        }

    }
    public void start_recieving()
    {
        reciever = new Thread(new ThreadStart(recv));
        reciever.Start();
    }

    public bool is_recieving()
    {
        try
        {
            if (reciever != null)
            {
                return reciever.IsAlive;
            }
            else
            {
                return false;
            }
        }
        catch(Exception e)
        {
            //Console.WriteLine(err.Message);
            return false;
        }
    }

    public TcpClient tcp_client = new TcpClient();


    private
        DateTime last_message_time;
        DateTime last_send_time;
        TimeSpan elapsed;
        Thread reciever;
        NetworkStream stream;

}

