﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public struct PackedMsg
{
	public List<char> dataPacket;
	public MessageInfo info;
};

public enum MessageInfo
{
	// from 0 to 255
	NOT_TRANSLATED_YET = 0,
	SET_REFERENCE = 10,
	GET_REFERENCE = 20,
	IS_CONTROLLER_CONNECTED = 30,
	IS_VISION_CONNECTED = 40,
	GET_FAULT_CODE = 50,
	ROBOT_MOVE = 60,
	GET_STATUS = 65,
	SET_VELOCITY = 70,
	GET_MAX_VELOCITY = 80,
	GET_MODE = 85,
	GET_ROBOT_VAR = 90,
	GET_ROBOT_POSITION = 100,
	ROBOT_RUN = 110,
	ROBOT_STOP = 120,
	GET_LOADED_PROG_NAME = 130,
	SET_SETTINGS = 140,
	STATUS_RUN = 150,
	STATUS_STOPPED = 160,
	STATUS_FAULTED = 170,
	OTHER = 255
};

public enum Mode
{
	NOT_SELECTED = 1,
	T1 = 50,
	AUTO = 100,
	T1_WITH_VISION = 150,
	AUTO_WITH_VISION = 200,
	WORLD = 210,
	JOINT = 220
}

public class DataPacketTranslator {
	public PackedMsg TranslateIncomingMsg(PackedMsg data) {
		// TO DO
		PackedMsg translatedMsg = new PackedMsg();

		switch ( (MessageInfo)data.dataPacket[0])
		{
			case MessageInfo.GET_MODE:
				translatedMsg.info = MessageInfo.GET_MODE;
				Console.WriteLine("rozpoznano wiadomosc - otrzymano get mode");
				break;
			default:
				translatedMsg.info = MessageInfo.OTHER;
			break;

		}
		return translatedMsg;
	}

	public PackedMsg getMode() {
		PackedMsg outComingMsg = new PackedMsg();
		outComingMsg.dataPacket.Add((char)MessageInfo.GET_MODE);
		Console.WriteLine("przetlumaczono wiadomosc - request get mode");
		return outComingMsg;
	}

}
    

