using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Net;
using System.Net.Sockets; //used for dll [not anymore, since linux compatibilty]
using System.Threading;

public struct Limit {
	double max;
	double min;
};
public struct DeltaParams {
	public double r1; 		// Arm length
	public double r2; 		// Forearm length
	public double rb; 		// Base radius
	public double re; 		// Effector radius
	public double max_vel;	// Maximal velocity move
	public Limit limit; 	// Limit theta angle
};
public struct Checkpoint
{
	public double x;
	public double y;
	public double z;
};

public struct Delta_vector
{
	public double x;
	public double y;
	public double z;
}
public struct listener_data
{
	public bool new_sim_command;
	public string sim_command;
}
public class DeltaModel
{
	public enum FAULT
	{
		OK = 0,
		NO_SOLUTION
	};
	public enum STATUS
	{

	};

	double r1;      // Arm length
	double r2;      // Forearm length
	double rb;      // Base radius
	double re;      // Effector radius
	double max_vel; // Maximal velocity move
	Limit limit;    // Limit theta angle

	STATUS status;
	FAULT fault_code;
	uint reference;
	uint mode;
	double vel;
	double step;
	double[] pos = new double[3];
	double[] jog = new double[3];
	bool MOVE_FLAG = false;
	bool JOG_or_other = false;
	double[] f_bend = new double[3];
	double[] f_twist = new double[3];
	readonly double const_2_sqrt3 = 2 / Math.Sqrt(3);
	readonly double const_sqrt3_3 = Math.Sqrt(3) / 3;
	List<Checkpoint> Trace = new List<Checkpoint>();

    readonly double max_elapsed_since_last_command = 50.0;
	DateTime last_command_time;
	DateTime exec_trace_time;
	TimeSpan elapsed_since_last_command;
	public Frame user_frame;

	TcpListener tcp_listener = null;
	public bool new_sim_command = false;
	public string sim_command = "";
	System.Threading.Thread listener;

	/*
	public const string cpp_circular_trajectory_dll = @"Circular_trajectory\x64\Debug\Circular_trajectory.dll"; //importing dll
	[DllImport(cpp_circular_trajectory_dll, CallingConvention = CallingConvention.Cdecl)]
	public static extern void calc_circ_data(Checkpoint start, Checkpoint aux, Checkpoint end, double[] result);

	[DllImport(cpp_circular_trajectory_dll, CallingConvention = CallingConvention.Cdecl)]
	public static extern double calc_angle(Delta_vector vector1, Delta_vector vector2); //implementation of .dll for calculating parameters
	*/

	public DeltaModel(DeltaParams args)
	{
		this.r1 = args.r1;
		this.r2 = args.r2;
		this.rb = args.rb;
		this.re = args.re;
		this.limit = args.limit;

		set_reference("JOINT");
		set_step(1);

		/* TEMP */
		pos[0] = 0;
		pos[1] = 0;
		pos[2] = -500;
		calc_ikine(pos, ref jog);
		/* TEMP */

		user_frame = new Frame("UF1", -500, 0, 0, 0, 0, 0);
	}
	public int get_status()
	{
		return (int)status;
	}
	public int get_fault_code()
	{
		return (int)fault_code;
	}
	public void ack_fault()
	{
		fault_code = FAULT.OK;
	}
	public void set_mode(string mode)
	{
		if (mode == "T1")
		{
			this.mode = 0;
			abort_listener();
		}
		else if (mode == "AUTO")
		{
			this.mode = 1;
			abort_listener();
		}
		else if (mode == "SIM")
		{
			this.mode = 2;
			setup_listener();
		}

	}
	public string get_mode()
	{
		switch (mode)
		{
			case 0: return "T1";
			case 1: return "AUTO";
			case 2: return "SIM";
			default: return "";
		}
	}
	public void set_reference(string reference)
	{
		if (reference == "JOINT")
		{
			this.reference = 0;
		}
		else if (reference == "WORLD")
		{
			this.reference = 1;
		}
		else if (reference == "UF_1")
		{
			this.reference = 2;
		}
	}
	public string get_reference()
	{
		switch (reference)
		{
			case 0: return "JOINT";
			case 1: return "WORLD";
			case 2: return "UF_1";
			default: return "";
		}
	}
	public void set_step(double step)
	{
		this.step = step;
	}
	public double[][] get_position()
	{
		double[][] position = { pos, jog };
		return position;
	}
	public void set_velocity(double vel)
	{
		this.vel = vel;
	}
	public double get_velocity()
	{
		return vel;
	}
	public double get_max_velocity()
	{
		return max_vel;
	}
	public bool get_MOVE_FLAG()
	{
		return MOVE_FLAG;

	}
	private double to_num_5(string value)
	{
		double num = 0;
		if (value[0] == 'V')
		{
			num += char.GetNumericValue(value[1]) * 1000;
			num += char.GetNumericValue(value[2]) * 100;
			num += char.GetNumericValue(value[3]) * 10;
			num += char.GetNumericValue(value[4]);
		}
		else
		{
			num += char.GetNumericValue(value[1]) * 100;
			num += char.GetNumericValue(value[2]) * 10;
			num += char.GetNumericValue(value[3]) * 1;
			num += char.GetNumericValue(value[4]) * 0.1;
			if (value[0] == '-')
				num = -num;
		}
		return num;
	}
	private int to_num_2(string value)
	{
		int num = (int)char.GetNumericValue(value[1]);
		if (value[0] == '-')
			num = -num;
		return num;
	}
	
	private string to_ret_string(double value)
    {
		value = Math.Round(value,1)*10;
		if (value >= 0)
		{
			return value.ToString("+0000");
        }
        else
        {
			return value.ToString("0000");
        }

	}

	/*
	 * Standard Commands: LIN, JNT, CIR, JOG
	 * LIN, JNT and CIR have variable speed
	 * 
	 * example commands:
	 * LIN+2500+1000-6800TOOL_ -  move to X=+2500 Y=+1000 Z=-6800 with default step 10
	 * CIRA+1000-2500-4500E-2500+1000-4500TOOL_V0300 - circular movement through Auxilary point to End point with speed 300 rad/s (fast)
	 * 
	 * TODO: change to enum based communication - data packet Translator
	 */
	public string command(string command)
	{
		string ret = command;
		JOG_or_other = false;
		if (command.Length > 2)
		{
			string instruction = command.Substring(0, 3);
			if (String.Equals(instruction, "LIN"))
			{
				JOG_or_other = false;
				string x = command.Substring(3, 5);
				string y = command.Substring(8, 5);
				string z = command.Substring(13, 5);
				if (command.Length > 23){
					string vel = command.Substring(23, 5);
					move_lin(to_num_5(x), to_num_5(y), to_num_5(z), to_num_5(vel));
                }else{
					move_lin(to_num_5(x), to_num_5(y), to_num_5(z));
				}
			}
			if (String.Equals(instruction, "JOG"))
			{
				JOG_or_other = true;
				last_command_time = DateTime.Now;
				//Console.WriteLine(command);
				string x = command.Substring(3, 2);
				string y = command.Substring(5, 2);
				string z = command.Substring(7, 2);
				move_JOG(to_num_2(x), to_num_2(y), to_num_2(z));
			}
			if (String.Equals(instruction, "CIR"))
			{
				JOG_or_other = false;
				//last_command_time = DateTime.Now;
				Console.WriteLine(command);
				Checkpoint aux = new Checkpoint();
				Checkpoint End = new Checkpoint();

				aux.x = to_num_5(command.Substring(4, 5));
				aux.y = to_num_5(command.Substring(9, 5));
				aux.z = to_num_5(command.Substring(14, 5));

				End.x = to_num_5(command.Substring(20, 5));
				End.y = to_num_5(command.Substring(25, 5));
				End.z = to_num_5(command.Substring(30, 5));

				if (command.Length > 40)
				{
					Console.WriteLine(command.Substring(40, 5));
					double vel = to_num_5(command.Substring(40, 5));
					move_Circ(aux, End, vel);
				}
				else
				{
					move_Circ(aux, End);
				}
				
			}
			if (String.Equals(instruction, "JNT"))
			{
				JOG_or_other = false;
				string x = command.Substring(3, 5);
				string y = command.Substring(8, 5);
				string z = command.Substring(13, 5);
				if (command.Length > 23)
				{
					string vel = command.Substring(23, 5);
					move_jnt(to_num_5(x), to_num_5(y), to_num_5(z), to_num_5(vel));
				}
				else
				{
					move_jnt(to_num_5(x), to_num_5(y), to_num_5(z));
				}
			}
			if (String.Equals(instruction, "G_P"))
			{
				ret = "POS" + to_ret_string(pos[0]) + to_ret_string(pos[1]) + to_ret_string(pos[2]) + "ROBOT";
			}
		}
		return ret;
	}
	public void check_for_commands()
    {
		if(new_sim_command)
        {
			new_sim_command = false;
			if (this.mode == 2)
			{
				command(sim_command);
			}
        }
    }
	public void move(int j1, int j2, int j3)
	{
		if (fault_code != FAULT.OK)
		{
			return;
		}

		int status = 0;
		double[] tmp_pos = pos;
		double[] tmp_jog = jog;
        double[] new_pos = { tmp_pos[0], tmp_pos[1], tmp_pos[2] };

		if (reference == 0)
		{
			tmp_jog[0] += step * (Math.PI / 180) * j1;
			tmp_jog[1] += step * (Math.PI / 180) * j2;
			tmp_jog[2] += step * (Math.PI / 180) * j3;
			//            tmp_jog[0] = -0.27;
			//            tmp_jog[1] = -0.27;
			//            tmp_jog[2] = -0.27;

			status = calc_fkine(ref tmp_pos, tmp_jog);
			//pos = tmp_pos;

		}
		else if (reference == 1)
		{
			// j1 = X(LEFT) j2 = Y(UP) j3 = Z(FRONT)
			new_pos[0] += step * j1;
			new_pos[1] += step * (j2);
			new_pos[2] += step * j3;

			if(in_limits(new_pos) && new_pos[2] >=-700 && new_pos[2]<=-320) {
				tmp_pos[0] = new_pos[0];
				tmp_pos[1] = new_pos[1];
				tmp_pos[2] = new_pos[2];
            }
			status = calc_ikine(tmp_pos, ref tmp_jog);

		}
		else if (reference == 2)
		{

			double[] jogVec = { j1, j2, j3 };
			jogVec = user_frame.get_move_vector(jogVec);
			tmp_pos[0] += step * jogVec[0];
			tmp_pos[1] += step * jogVec[1];
			tmp_pos[2] += step * jogVec[2];

			status = calc_ikine(tmp_pos, ref tmp_jog);

			// j1 = X(LEFT) j2 = Y(UP) j3 = Z(FRONT)
		}
	}
	public void move_to(double j1, double j2, double j3)
	{
		double[] new_pos = { j1, j2, j3 };
		if (in_limits(new_pos))
		{
			if (fault_code != FAULT.OK)
			{
				return;
			}
			int status = 0;
			double[] tmp_pos = pos;
			double[] tmp_jog = jog;

			if (reference == 1 || reference == 0)
			{
				tmp_pos[0] = j1;
				tmp_pos[1] = j2;
				tmp_pos[2] = j3;
				status = calc_ikine(tmp_pos, ref tmp_jog);
			}

			switch (status)
			{
				case -1: fault_code = FAULT.NO_SOLUTION; break;
				case 0:
					{
						pos = tmp_pos;
						jog = tmp_jog;
						break;
					}
				case 1:
					{
						MOVE_FLAG = false;
						break;
					}
			}
		}

	}

	public void move_lin(double j1, double j2, double j3, double vel = 10)	{
		if(this.mode == 2 && !MOVE_FLAG)
		{
			MOVE_FLAG = true;
			calc_trace_lin(pos[0], pos[1], pos[2], j1, j2, j3,vel);
        }
	}
	public void move_jnt(double j1, double j2, double j3, double vel = 10)
	{
		if (this.mode == 2 && !MOVE_FLAG)
		{
			MOVE_FLAG = true;
			calc_trace_jnt(pos[0], pos[1], pos[2], j1, j2, j3, vel);
		}
	}

	public void move_JOG(int j1, int j2, int j3)	{
		if (this.mode == 2 && !MOVE_FLAG)	{
			MOVE_FLAG = true;
			for (int i = 0; i <= 15; i++)	{
				Checkpoint new_checkpoint;
                new_checkpoint.x = pos[0] + j1*i;
                new_checkpoint.y = pos[1] + j2*i;
                new_checkpoint.z = pos[2] + j3*i;
				if (new_checkpoint.z > -3200 || new_checkpoint.z < -7000)
				{
					Trace.Add(new_checkpoint);
				}
			}
		}

	}
	public void move_Circ(Checkpoint aux, Checkpoint end, double vel = 10)
	{
		Checkpoint start = new Checkpoint();
		start.x = pos[0];
		start.y = pos[1];
		start.z = pos[2];
		double[] circ_data = new double[4];
		if (this.mode == 2 && !MOVE_FLAG)
		{
			MOVE_FLAG = true;

			CircularCalculator.calc_circ_data(start, aux, end, ref circ_data);

			double u = circ_data[0];
			double v = circ_data[1];
			double w = circ_data[2];
			double r = circ_data[3];

			Delta_vector start_vector = new Delta_vector();
			Delta_vector aux_vector = new Delta_vector();
			Delta_vector end_vector = new Delta_vector();

			start_vector.x = start.x - u;
			start_vector.y = start.y - v;
			start_vector.z = start.z - w;

			aux_vector.x = aux.x - u;
			aux_vector.y = aux.y - v;
			aux_vector.z = aux.z - w;

			end_vector.x = end.x - u;
			end_vector.y = end.y - v;
			end_vector.z = end.z - w;
			//calculating angle between starting vector and auxilary vector...
			double alpha1 = CircularCalculator.calc_angle(start_vector, aux_vector);
			//so that Effector goes to end through aux and not the other way around.
			//calculating angle between auxilary vector and end vector
			double alpha2 = CircularCalculator.calc_angle(aux_vector, end_vector);

			double alpha = alpha1 + alpha2;
			//Console.WriteLine((alpha*180/Math.PI).ToString());
			double omega = vel / r; //
			int n = 1 + (int)(alpha / omega);

		//	Console.WriteLine(n.ToString());
			Checkpoint new_checkpoint = new Checkpoint();

			for (int k = 0; k <= n/2; k++)
			{
				double theta = k * alpha1 /(n/2);
				new_checkpoint.x = u + (Math.Sin(alpha1 - theta) * start_vector.x + Math.Sin(theta) * aux_vector.x) / Math.Sin(alpha1);
				new_checkpoint.y = v + (Math.Sin(alpha1 - theta) * start_vector.y + Math.Sin(theta) * aux_vector.y) / Math.Sin(alpha1);
				new_checkpoint.z = w + (Math.Sin(alpha1 - theta) * start_vector.z + Math.Sin(theta) * aux_vector.z) / Math.Sin(alpha1);
				Trace.Add(new_checkpoint);
			}

			new_checkpoint.x = aux.x;
			new_checkpoint.y = aux.y;
			new_checkpoint.z = aux.z;
			Trace.Add(new_checkpoint);

			for (int k = 0; k <= n/2; k++)
			{
				double theta = k * alpha2 / (n/2);
				new_checkpoint.x = u + (Math.Sin(alpha2 - theta) * aux_vector.x + Math.Sin(theta) * end_vector.x) / Math.Sin(alpha2);
				new_checkpoint.y = v + (Math.Sin(alpha2 - theta) * aux_vector.y + Math.Sin(theta) * end_vector.y) / Math.Sin(alpha2);
				new_checkpoint.z = w + (Math.Sin(alpha2 - theta) * aux_vector.z + Math.Sin(theta) * end_vector.z) / Math.Sin(alpha2);
				Trace.Add(new_checkpoint);
			}

			new_checkpoint.x = end.x;
			new_checkpoint.y = end.y;
			new_checkpoint.z = end.z;
			Trace.Add(new_checkpoint);
		}
	}
	public void exec_trace()
    {
		exec_trace_time = DateTime.Now;
		elapsed_since_last_command = (exec_trace_time - last_command_time);
		if (!JOG_or_other)
		{
			if (Trace.Count > 0 && this.mode == 2)
			{
				Checkpoint new_checkpoint = Trace[0];
				move_to(new_checkpoint.x, new_checkpoint.y, new_checkpoint.z);
				Trace.RemoveAt(0);
			}
			else
			{
				MOVE_FLAG = false;
			}
		}else
        {
			if (Trace.Count > 0 && this.mode == 2 && elapsed_since_last_command.TotalMilliseconds < max_elapsed_since_last_command)
			{
				Checkpoint new_checkpoint = Trace[0];
				move_to(new_checkpoint.x, new_checkpoint.y, new_checkpoint.z);
				Trace.RemoveAt(0);
			}
			else
			{
				Trace.Clear();
				MOVE_FLAG = false;
			}
		}

    }
	private void calc_trace_lin(double act_pos_j1, double act_pos_j2, double act_pos_j3, double cmd_pos_j1, double cmd_pos_j2, double cmd_pos_j3, double h = 1)
    {
		double[] direct = new double[3];
		direct[0] = cmd_pos_j1 - act_pos_j1;
		direct[1] = cmd_pos_j2 - act_pos_j2;
		direct[2] = cmd_pos_j3 - act_pos_j3;

		double length = Math.Sqrt(direct[0] * direct[0] + direct[1] * direct[1]+direct[2] * direct[2]);
		direct[0] = direct[0] / length;
		direct[1] = direct[1] / length;
		direct[2] = direct[2] / length;

		uint n = 1 + ((uint)(0.5 + length/h));
		h = length / n;

		this.Trace.Clear();
		Checkpoint p = new Checkpoint();
		for(int i = 0; i < n; i++)
        {
			p.x = act_pos_j1 + direct[0] * h * i;
			p.y = act_pos_j2 + direct[1] * h * i;
			p.z = act_pos_j3 + direct[2] * h * i;
			Trace.Add(p);
		}
		p.x = cmd_pos_j1;
		p.y = cmd_pos_j2;
		p.z = cmd_pos_j3;
		Trace.Add(p);
		//Console.WriteLine(Trace.Count);
	}

	private void calc_trace_jnt(double act_pos_j1, double act_pos_j2, double act_pos_j3, double cmd_pos_j1, double cmd_pos_j2, double cmd_pos_j3, double vel = 1)
	{
		double[] end_ang = new double[3];
		double[] act_ang = new double[3];
		double[] act_pos = new double[3];
		double[] end_pos = new double[3];

		double[] angles = new double[3];

		double[] direct = new double[3];
		direct[0] = cmd_pos_j1 - act_pos_j1;
		direct[1] = cmd_pos_j2 - act_pos_j2;
		direct[2] = cmd_pos_j3 - act_pos_j3;

		//double length = Math.Sqrt(direct[0] * direct[0] + direct[1] * direct[1] + direct[2] * direct[2]);
		double biggest_angle = 0.0;
		act_pos[0] = act_pos_j1;
		act_pos[1] = act_pos_j2;
		act_pos[2] = act_pos_j3;

		end_pos[0] = cmd_pos_j1;
		end_pos[1] = cmd_pos_j2;
		end_pos[2] = cmd_pos_j3;

		calc_ikine(act_pos, ref act_ang);
		calc_ikine(end_pos, ref end_ang);

		angles[0] = end_ang[0] - act_ang[0];
		angles[1] = end_ang[1] - act_ang[1];
		angles[2] = end_ang[2] - act_ang[2];

		if (angles[0] >= angles[1] && angles[0] >= angles[2])
		{
			biggest_angle = angles[0];
		}
		else if (angles[1] >= angles[0] && angles[1] >= angles[2])
		{
			biggest_angle = angles[1];
		}
		else if (angles[2] >= angles[0] && angles[2] >= angles[1])
		{
			biggest_angle = angles[2];
		}


		//uint n = 1 + ((uint)(0.5 + length / vel));
		uint n = 1 + (uint)(0.5 + biggest_angle * 200 / vel);
		this.Trace.Clear();
		Checkpoint p = new Checkpoint();
		for (int i = 0; i < n; i++)
		{
			double[] curr_angle = new double[3];
			double[] curr_pos = new double[3];
			curr_angle[0] = act_ang[0] + (double)(angles[0] / n) * i;
			curr_angle[1] = act_ang[1] + (double)(angles[1] / n) * i;
			curr_angle[2] = act_ang[2] + (double)(angles[2] / n) * i;
			calc_fkine(ref curr_pos, curr_angle);

			p.x = curr_pos[0];
			p.y = curr_pos[1];
			p.z = curr_pos[2];

			Trace.Add(p);
		}
		p.x = end_pos[0];
		p.y = end_pos[1];
		p.z = end_pos[2];

		Trace.Add(p);


	}

	private int calc_fkine(ref double[] pos, double[] th)
	{
		//pos[0] = pos[1] = pos[2] = 0.0;
		double[] theta = { th[0], th[1], th[2] };

		double f = rb / 0.5 / 0.57735;
		double e = re / 0.5 / 0.57735;
		double t = (f - e) * Math.Tan(Math.PI / 6) / 2.0;
		double dtr = Math.PI / 180.0;

		theta[0] = -theta[0];//*dtr;
		theta[1] = -theta[1];//*dtr;
		theta[2] = -theta[2];//*dtr;

		double y1 = -(t + r1 * Math.Cos(theta[0]));
		double z1 = -r1 * Math.Sin(theta[0]);
		//		GD.Print("1 ====");
		//		GD.Print(y1);
		//		GD.Print(z1);

		double y2 = (t + r1 * Math.Cos(theta[1])) * Math.Sin(Math.PI / 6);
		double x2 = y2 * Math.Tan(Math.PI / 3);
		double z2 = -r1 * Math.Sin(theta[1]);

		//		GD.Print("2 ====");
		//		GD.Print(x2);
		//		GD.Print(y2);
		//		GD.Print(z2);

		double y3 = (t + r1 * Math.Cos(theta[2])) * Math.Sin(Math.PI / 6);
		double x3 = -y3 * Math.Tan(Math.PI / 3);
		double z3 = -r1 * Math.Sin(theta[2]);
		//		GD.Print("3 ====");
		//		GD.Print(x3);
		//		GD.Print(y3);
		//		GD.Print(z3);

		double dnm = (y2 - y1) * x3 - (y3 - y1) * x2;

		//		GD.Print("dnm ====");
		//			GD.Print(dnm);

		double w1 = y1 * y1 + z1 * z1;
		double w2 = x2 * x2 + y2 * y2 + z2 * z2;
		double w3 = x3 * x3 + y3 * y3 + z3 * z3;
		//		GD.Print("w ====");
		//		GD.Print(w1);
		//		GD.Print(w2);
		//		GD.Print(w3);

		/* x = (a1*z + b1)/dnm */
		double a1 = (z2 - z1) * (y3 - y1) - (z3 - z1) * (y2 - y1);
		double b1 = -((w2 - w1) * (y3 - y1) - (w3 - w1) * (y2 - y1)) / 2.0;
		//		GD.Print("ab1 ====");
		//		GD.Print(a1);
		//		GD.Print(b1);

		/* y = (a2*z + b2) / dnm */
		double a2 = -(z2 - z1) * x3 + (z3 - z1) * x2;
		double b2 = ((w2 - w1) * x3 - (w3 - w1) * x2) / 2.0;
		//		GD.Print("ab2 ====");
		//		GD.Print(a2);
		//		GD.Print(b2);

		/* a*z^2 + b*z + c = 0 */
		double a = a1 * a1 + a2 * a2 + dnm * dnm;
		double b = 2.0 * (a1 * b1 + a2 * (b2 - y1 * dnm) - z1 * dnm * dnm);
		double c = (b2 - y1 * dnm) * (b2 - y1 * dnm) + b1 * b1 + dnm * dnm * (z1 * z1 - r2 * r2);
		//		GD.Print("eh1 ====");
		//		GD.Print((b2 - y1*dnm)*(b2 - y1*dnm));
		//		GD.Print("eh2 ====");
		//		GD.Print( b1*b1 + dnm*dnm*(z1*z1 - r2*r2) );
		//
		//		GD.Print("abc ====");
		//		GD.Print(a);
		//		GD.Print(b);
		//		GD.Print(c);

		double d = b * b - 4.0 * a * c;
		//		GD.Print("d ====");
		//		GD.Print(d);

		double[] new_pos = { (a1 * pos[2] + b1) / dnm,
							(a2 * pos[2] + b2) / dnm,
							-0.5 * (b + Math.Sqrt(d)) / a};

		if (in_limits(new_pos))
		{
			pos[2] = -0.5 * (b + Math.Sqrt(d)) / a;
			pos[0] = (a1 * pos[2] + b1) / dnm;
			pos[1] = (a2 * pos[2] + b2) / dnm;
		}
		//Console.WriteLine("X: " + pos[0] + "  Y: " + pos[1] + "  Z: " + pos[2]);
		return 0;
	}
	private int calc_ikine(double[] pos, ref double[] th)
	{
		if (in_limits(pos))
		{
			th[0] = th[1] = th[2] = 0.0;

			double[] pos120 = new double[3];
			pos120[0] = pos[0] * Math.Cos(4 * Math.PI / 6) + pos[1] * Math.Sin(4 * Math.PI / 6);
			pos120[1] = pos[1] * Math.Cos(4 * Math.PI / 6) - pos[0] * Math.Sin(4 * Math.PI / 6);
			pos120[2] = pos[2];
			double[] pos240 = new double[3];
			pos240[0] = pos[0] * Math.Cos(4 * Math.PI / 6) - pos[1] * Math.Sin(4 * Math.PI / 6);
			pos240[1] = pos[1] * Math.Cos(4 * Math.PI / 6) + pos[0] * Math.Sin(4 * Math.PI / 6);
			pos240[2] = pos[2];

			int status = calc_angle(pos, ref th[0]);
			if (status == 0)
			{
				status = calc_angle(pos120, ref th[1]);
			}
			if (status == 0)
			{
				status = calc_angle(pos240, ref th[2]);
			}
			//	Console.WriteLine("x: " + pos[0] + "  y: " + pos[1] + " z: " + pos[2]);

			return status;
		}
		else
        {
			return 1;
        }
	}
	private int calc_angle(double[] p, ref double alpha)
	{
		double ym = -rb;//-0.5 * 0.57735 * rb;//-rb;// * 0.5 * Math.Tan(Math.PI / 6);			// motor[1]
		double yh =  p[1] - re;//p[1] - 0.5 * 0.57735 * re;//p[1] - re;// * 0.5 * Math.Tan(Math.PI / 6);	// hand[1]


		/* z = a + b*y */
		double a = (p[0] * p[0] + yh * yh + p[2] * p[2] + r1 * r1 - r2 * r2 - ym * ym) / (2.0 * p[2]);
		double b = (ym - yh) / p[2];

		double d = -(a + b * ym) * (a + b * ym) + r1 * (b * b * r1 + r1);
		if (d < 0)
		{
			return -1; // NO_SOLUTION
		}

		double yj = (ym - a * b - Math.Sqrt(d)) / (b * b + 1);
		double zj = a + b * yj;

		alpha = Math.Atan2((zj), (ym - yj));// * 180 / Math.PI;;
		return 0;
	}

	private bool in_limits(double[] pos)
    {
		double alpha1 = (Math.Asin(Math.Abs(pos[0])/r2)/Math.PI)*180;

		double d2 = Math.Abs(const_sqrt3_3 * pos[0] - pos[1]) /const_2_sqrt3;
		double alpha2 = (Math.Asin(d2 / r2) / Math.PI) * 180;

		double d3 = Math.Abs(const_sqrt3_3 * pos[0] + pos[1]) / const_2_sqrt3;
		double alpha3 = (Math.Asin(d3 / r2) / Math.PI) * 180;

        if (alpha1 < 40.0 && alpha2 < 40.0 && alpha3 < 40.0 && pos[2]>=-700 && pos[2]<=-320) {
			return true;
        }else {
			return false;
        }
    }
	//command listener

	private void setup_listener()
    {
		Int32 port = 2137;
		IPAddress localAddr = IPAddress.Parse("127.0.0.1");

		// TcpListener server = new TcpListener(port);
		tcp_listener = new TcpListener(localAddr, port);
		listener = new System.Threading.Thread(new ThreadStart(listen));
		listener.Start();
	}

	private void abort_listener()
    {
		if (tcp_listener != null)
		{
            if (listener.IsAlive)
            {
				listener.Abort();
				tcp_listener.Stop();
            }
		}

    }

    private void listen()
    {
		tcp_listener.Start();
		while(true)
        {
			try
			{
				Byte[] bytes = new Byte[256];
				String data = null;

				while (true)
				{
					Console.Write("Waiting for a connection... ");

					TcpClient client = tcp_listener.AcceptTcpClient();
					Console.WriteLine("Connected!");

					data = null;
					NetworkStream stream = client.GetStream();

					int i;

					// Loop to receive all the data sent by the client.
					while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
					{
						// Translate data bytes to a ASCII string.
						data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
						string ret = command(data);

						byte[] msg = System.Text.Encoding.ASCII.GetBytes(ret);

						stream.Write(msg, 0, msg.Length);
					}

					// Shutdown and end connection
					client.Close();
				}
			}
			catch (SocketException e)
			{
				Console.WriteLine("SocketException: {0}", e);
			}
			finally
			{
				// Stop listening for new clients.
				tcp_listener.Stop();
			}

		}

	}
}