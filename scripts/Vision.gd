extends Node2D

onready var capture = get_node("Cap");

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _process(_delta):
	var newimage = Image.new();
	newimage.create_from_data(426,240,false,Image.FORMAT_RGB8,Proxy.current_frame);
	var texture = ImageTexture.new();
	texture.create_from_image(newimage);
	capture.texture=texture;
