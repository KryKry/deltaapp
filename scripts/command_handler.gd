extends Node
var X = "X"
var Y = "Y"
var Z = "Z"
var Point
var Kord
var n = 1
var N
var line = 0;
var test
var list
enum {
	ARG_INT
	ARG_STRING
	ARG_BOOL
	ARG_FLOAT
}

func _ready():
	pass

#funkcja zapisuje punkt do pliku txt
func save_file(text):
	var file = File.new()
	file.open("res://saved_data.txt",File.READ_WRITE)
	file.seek_end()
	file.store_line(text)
	test = file.get_position()
	file.close()
	line = test

#funkcja help
func help():
	list = List.commands
	return list

#zapisywanie wybranych punktów
func save_point():
	Point = Proxy.get_robot_position()
	Kord = Point as String
	N = n as String
	save_file("Punkt " + N + "\n" + Kord + "\n")
	n = n + 1
	return Point

