using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Drawing;


public class UDPClientHandler
{
    private UdpClient client;
    private IPEndPoint endPoint;
    private Thread reciever;
    public void Connect()
    {
        client = new UdpClient();
        client.Connect("127.0.0.1", 2013);   
        try
        {
            Byte[] sendBytes = Encoding.ASCII.GetBytes(" ");
            client.Send(sendBytes, sendBytes.Length);
        }
        catch(Exception e)
        {
            Console.WriteLine("{0} Exception caught.", e);
        }
    }
    
    private void recieve_data()
    {
        while (true)
        {
            //old

            Byte[] ret = new byte[306720];
            endPoint = new IPEndPoint(IPAddress.Any, 0);
            int position;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    Byte[] receiveBytes = client.Receive(ref endPoint);
                    byte number = receiveBytes.First();
                    position = Convert.ToInt32(number);
                    Buffer.BlockCopy(receiveBytes, 1, Proxy.recieved_frame, 0 + position * 61344, 61344);
                    //Console.WriteLine("got packet no "+i.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Exception caught.", e);
                }

            }

            /*
            endPoint = new IPEndPoint(IPAddress.Any, 0);
            Byte[] msg_len_bytes = new byte[4];
            Mat img = new Mat();    
            int msg_len;
            Byte[] receiveBytes = client.Receive(ref endPoint);
            Buffer.BlockCopy(receiveBytes, 0,msg_len_bytes, 0, 4);
            PrintByteArray(msg_len_bytes);
            msg_len = BitConverter.ToInt32(msg_len_bytes,0);
            Byte[] buff = new byte[msg_len];
            Buffer.BlockCopy(receiveBytes, 4, buff, 0, msg_len);
            img = Cv2.ImDecode(buff, ImreadModes.Color);
            //Proxy.current_frame = buff;*/
        }
    }

    public void on_quit()
    {
        if(client != null)
            client.Close();
        if(reciever != null)
        reciever.Abort();
    }

    public bool is_connected()
    {
        bool ret = true;
        try
        {
            Byte[] receiveBytes = client.Receive(ref endPoint);
        }
        catch(Exception e)
        {
            //Console.WriteLine("{0} Exception caught.", e);
            ret=false;
        }
        return ret;
    }
    public void recieve()
    {
        reciever = new Thread(new ThreadStart(recieve_data));
        reciever.Start();
    }
    public void PrintByteArray(byte[] bytes)
    {
        var sb = new StringBuilder("new byte[] { ");
        foreach (var b in bytes)
        {
            sb.Append(b + ", ");
        }
        sb.Append("}");
        //Console.WriteLine(sb.ToString());
    }
}