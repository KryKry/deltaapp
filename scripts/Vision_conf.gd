extends Viewport

func _ready():
	set_process_input(true)
	set_process_unhandled_input(true)

func _input(event):
	unhandled_input(event)
