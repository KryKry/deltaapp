extends Spatial

class_name DeltaView

var Delta : Spatial;
var Arm : Array;
var ForearmL : Array;
var ForearmR : Array;
var Effector : Spatial;
var Axes_main : Spatial;
var Axes_aux : Spatial;

var Rb = 204.3; 		# Base radius
var Re = 45;		# Effector radius
var R1 = 280;		# Arm radius
var R2 = 590;		# Forearm radius
var Lj = 67.5;		# Elbow offset
var Le = 41;		# Effector offset

var transform_handle: Array;

func _init(var node : Spatial):
	Delta = node;
	Arm.append(Delta.get_node("J1/Arm"));
	Arm.append(Delta.get_node("J2/Arm"));
	Arm.append(Delta.get_node("J3/Arm"));
	
	ForearmL.append(Delta.get_node("J1/Arm/ForearmL"));
	ForearmL.append(Delta.get_node("J2/Arm/ForearmL"));
	ForearmL.append(Delta.get_node("J3/Arm/ForearmL"));

	ForearmR.append(Delta.get_node("J1/Arm/ForearmR"));
	ForearmR.append(Delta.get_node("J2/Arm/ForearmR"));
	ForearmR.append(Delta.get_node("J3/Arm/ForearmR"));

	Effector = Delta.get_node("Effector");
	
	Axes_main = Delta.get_node("Axes_3_main");
	Axes_aux = Delta.get_node("Axes_3_aux");
	
	#var front = [
	#	Vector3(1, 0, 0),
	#	Vector3(-0.5, 0, -0.866026),
	#	Vector3(-0.5, 0, 0.866026)
	#];
	
#	var front = [
#		Vector3(-1, 0, 0),
#		Vector3(-0.5, 0, 0.866026),
#		Vector3(0.5, 0, 0.866026)
#	];
	var front = [
		Vector3(0, 0, -1),
		Vector3(-0.866026, 0, 0.5),
		Vector3(0.866026, 0, 0.5)
	];

	var right = [
		Vector3( front[0].cross(Vector3(0,1,0)).normalized() ),
		Vector3( front[1].cross(Vector3(0,1,0)).normalized() ),
		Vector3( front[2].cross(Vector3(0,1,0)).normalized() )
	];
	transform_handle = [
		(front[0] * Re) - (right[0] * Le),
		(front[0] * Re) + (right[0] * Le),
		(front[1] * Re) - (right[1] * Le),
		(front[1] * Re) + (right[1] * Le),
		(front[2] * Re) - (right[2] * Le),
		(front[2] * Re) + (right[2] * Le),
	];
	Axes_main.translation = Vector3(0.0,0.0,0.0);
	Axes_main.rotation_degrees = Vector3(0.0,270.0,0.0);
	Axes_aux.translation = Vector3(0.0,0.0,0.0);
	Axes_aux.rotation_degrees = Vector3(0.0,270.0,0.0);
	
	
func update(var val) -> bool:
	var pos = Vector3(val[0][0], val[0][1], val[0][2]);
	var handle = [
		[ pos + transform_handle[0], pos - transform_handle[1] ],
		[ pos + transform_handle[2], pos - transform_handle[3] ],
		[ pos + transform_handle[4], pos - transform_handle[5] ]
	];
	#var tmp_pos = [pos[1], pos[2], pos[0]];
	var tmp_pos = Vector3(-val[0][2], val[0][1], val[0][0]);
	Effector.translation = tmp_pos;
	
#	print(handle)
	for i in range(0, 3):
		Arm[i].rotation = Vector3(val[1][i], 0 ,0);
		
		var joint = Vector3(
			ForearmL[i].transform.origin.x,
			ForearmL[i].transform.origin.y,
			ForearmL[i].transform.origin.z - Rb
			);
		var up = ForearmL[i].transform.basis.x.cross(joint);
		var target = [
			handle[i][0] + Delta.global_transform.origin,
			handle[i][1] + Delta.global_transform.origin
			];
		ForearmL[i].look_at(target[0], up);
		ForearmR[i].look_at(target[1], up);

	return integrity(pos, handle);



func integrity(var _pos, var handle) -> bool:
	var obj = [
		[ ForearmL[0], ForearmR[0] ],
		[ ForearmL[1], ForearmR[1] ],
		[ ForearmL[2], ForearmR[2] ]
	];
	
	for i in range(6):
		for j in range(2):
			var sim : Vector3;
			sim += obj[i][j].transform.basis.z * (-R2);
			sim += obj[i][j].global_transform.origin;
			if( sim.distance_to(handle[i][j]) > 0 ):
				return false;
	return true

func rebase_axes(var vect_pos, var vect_rot):
	Axes_aux.translation = vect_pos;
	Axes_aux.rotation_degrees = Vector3(vect_rot[0], vect_rot[1]+270.0, vect_rot[2]);
	
