extends Node2D
class Line:
	var id
	var color
	var thickness
	var a3d = Vector3()
	var b3d = Vector3()
	var a = Vector2()
	var b = Vector2()
var Lines
var CameraNode
	
func _ready():
	CameraNode = get_viewport().getcamera()
	Lines = []
	set_process(true)

func draw():
	for line in Lines:
		line.a = CameraNode.unprojectposition(line.a3d)
		line.b = CameraNode.unprojectposition(line.b3d)
		draw_line(line.a, line.b, line.color, line.thickness)
	
func _process(delta):
	update()

func DrawLine3D(id, vectora, vectorb, color, thickness):
	for line in Lines:
		if line.id == id:
			line.color = color
			line.a3d = vectora
			line.b3d = vectorb
			line.a = CameraNode.unprojectposition(vectora)
			line.b = CameraNode.unprojectposition(vectorb)
			line.thickness = thickness
		return
	var newline = Line.new()
	newline.id = id
	newline.color = color
	newline.a = CameraNode.unprojectposition(vectora)
	newline.b = CameraNode.unprojectposition(vectorb)
	newline.thickness = thickness
	Lines.append(newline)

func Remove_Line(id):
	var i = 0
	var found = false
	for line in Lines:
		if line.id == id:
			found = true
			break
	i += 1
	if found:
		Lines.remove(i)
