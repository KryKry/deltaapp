extends Spatial

var n = 0;
var x;
func _ready():
	get_node("Effector/Axes_3_effector").hide()

func _on_Area_input_event(_camera, event, _position, _normal, _shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed == true:
			n = n + 1;
			print(n);
			print(x);
	x = n % 2;
	if x == 1:
		get_node("Effector/Axes_3_effector").show()
	if x == 0:
		get_node("Effector/Axes_3_effector").hide()
