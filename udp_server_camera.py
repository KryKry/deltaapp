import socket
import threading
import numpy as np
import cv2 as cv
import time

#serwer
ip = "127.0.0.1"
port = 2013
server = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 65536)
server.bind((ip,port))
cap = cv.VideoCapture(0)
def handle_client(*address):
    #12 pakietow po 57600 bajtow - 360p
    #5 pakietow po 61344 - 240p
    while True:
        while (True):
            ret, frame = cap.read()
            if ret == True:
                frame = cv.resize(frame,(426,240))
                #kod z UDP_server
                tosend = bytearray()
                zdj = frame.tolist() 
                for row in zdj:
                    for pixel in row:
                        tosend.append(int(pixel[2]))
                        tosend.append(int(pixel[1]))
                        tosend.append(int(pixel[0]))#godot przyjmuje format odwrotny
                #send = bytes(tex,'UTF-8')
                for j in range(1):
                    time0 = time.time()
                    for i in range(5):
                        sendy = bytearray([i])
                        sendy += tosend[i*61344:(i+1)*61344]
                        server.sendto(sendy, address)
                    time1 = time.time()
                    print("elapsed"+str(time1-time0))
            else:
                break
    cap.release()
    cv.destroyAllWindows()
    
def start():
    #server.listen()
    print("server listen on "+str(ip))
    while True:
        try:
            msg, address = server.recvfrom(65536)
            print("got connection at "+str(address))
            print(address)
            if msg != "t":
                
                thread=threading.Thread(target=handle_client,args=(address))
            
            thread.start()
        except ConnectionResetError:
            cap.release()
print("starting")
start()