import socket
import threading
import numpy 
import time

def get_pos():
    global position_act
    global stop_threads
    global robot_moving
    while not stop_threads:
        time.sleep(0.05)
        client.sendall("G_P".encode())
        position_string = client.recv(26).decode()
        #print(position_string)
        if(position_string[0:3]=="POS"):
            '''
            x = int(position_string[3:8])
            position_act[0] = x
            
            y = int(position_string[8:13])
            position_act[1] = y
            
            z = int(position_string[13:18])
            position_act[2] = z
            '''
            if(position_string[23:26]=="N_M"):
                print(position_string)
                robot_moving == False
            else:
                robot_moving == True
        
def start():
    print("starting Demo")
    #thread=threading.Thread(target=get_pos,args=())
    #thread.start()
    time.sleep(1)
    global position      
    global stop_threads
    for command in trace:
        if(command[0:3]=="TIM"):
            time.sleep((int(command[3:7])/1000))
        else:
            msg = command
            #print(msg)
            client.sendall(msg.encode())
            
            time.sleep(0.2)
            pos_string = client.recv(26).decode()
            robot_state = pos_string[23:26]
            
            while(robot_state == "I_M"):
                time.sleep(0.2)
                pos_string = client.recv(26).decode()
                robot_state = pos_string[23:26]
                
            print("Done")
            time.sleep(0.4)
    stop_threads = True
    
def position_string_to_position_array(position_string):
    position = [0,0,0]
    x = int(position_string[0:5])

    position[0] = x
    y = int(position_string[5:10])

    position[1] = y
    z = int(position_string[10:15])

    position[2] = z
    return position

def position_to_position_string(position):
    ret_val = ""
    for item in position:
        if item > 0:
            n = 4-len(str(item))
            comp ="+"+n*"0"+str(item)
        else:
            n= 4-len(str(-item))
            comp = "-"+n*"0"+str(-item)
        ret_val+=comp
    return ret_val

def get_end_position(command):
    if command[0:3] == "LIN" or command[0:3] == "JNT":
        return position_string_to_position_array(command[3:18])
    elif command[0:3] == "CIR":
        return position_string_to_position_array(command[20:35])
            
print("starting program")
position_act = [0,0,0]
trace = []
steps = open("DEMO.txt","r")
for command in steps:
    trace.append(command.strip("\n"))

stop_threads = False
robot_moving = False
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(("192.168.0.155",10))
start()
